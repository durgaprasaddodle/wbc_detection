#ifndef WBCSTRUCT_H
#define WBCSTRUCT_H
#include <string>
#include <vector>

namespace spin
{
  struct box
  {
    int x = 0;
    int y = 0;
    int width = 0;
    int height = 0;

  };

  struct point
  {
    int x;
    int y;
  };

  class WBCStruct
  {
    private:
      std::string aoi_name = "";
      std::vector<point> mono_regions;
      std::vector<point> wbc_cent;
      std::string region_tag = "";
      std::vector<float> wbc_area;
      std::vector<float> wbc_peri;
      std::vector<float> wbc_dia;
      std::vector<float> wbc_ecc;
      int num_wbc = 0;
      int mono_votes = 0;
      int fore_ratio = 0;
      float fg_val = 0;
      float TotalFG = 0;
      float TotalBG = 0;
    public:
      WBCStruct()
      {
      }
      ~WBCStruct()
      {
        wbc_area.clear();
        wbc_peri.clear();
        wbc_dia.clear();
        wbc_ecc.clear();
        mono_regions.clear();
        wbc_cent.clear();
      }

      void setFGVal(float v)
      {
        fg_val = v;
      }
      void setTotalFG(float tfg)
      {
        TotalFG = tfg;
      }
      void setTotalBG(float tbg)
      {
        TotalBG = tbg;
      }

      void setAoiName(std::string n)
      {
        aoi_name = n;
      }
      void setRegionTag(std::string s)
      {
        region_tag = s;
      }
      void setMonoVotes(int v)
      {
        mono_votes = v;
      }
      void setImageName(std::string s)
      {
        aoi_name = s;
      }
      void setMonoRegion(std::vector<point> p)
      {
        mono_regions = p;
      }
      void setNumberOfWBC(int w)
      {
        num_wbc = w;
      }
      void setWBCCentroid(std::vector<point> w_p)
      {
        wbc_cent = w_p;
      }
      void setWBCArea(std::vector<float> ar)
      {
        wbc_area = ar;
      }
      void setWBCPeri(std::vector<float> ar)
      {
        wbc_peri = ar;
      }
      void setWBCDia(std::vector<float> ar)
      {
        wbc_dia = ar;
      }
      void setWBCECC(std::vector<float> ar)
      {
        wbc_ecc = ar;
      }
      std::string GetAoiName()
      {
        return aoi_name;
      }
      int GetWBCCount()
      {
        return num_wbc;
      }
      std::string GetRegionType()
      {
        return region_tag;
      }
      std::vector<point> GetWBCCentroid()
      {
        return wbc_cent;
      }
      std::vector<point> GetMonoCentroid()
      {
        return mono_regions;
      }
      std::vector<float> GetWBCArea()
      {
        return wbc_area;
      }
      std::vector<float> GetWBCPeri()
      {
        return wbc_peri;
      }
      std::vector<float> GetWBCDia()
      {
        return wbc_dia;
      }
      std::vector<float> GetWBCECC()
      {
        return wbc_ecc;
      }
      int GetMonoVotes()
      {
        return mono_votes;
      }
      float GetFGVal()
      {
        return fg_val;
      }
      float GetTotalFG()
      {
        return TotalFG;
      }
      float GetTotalBG()
      {
        return TotalBG;
      }
  };
}//end of namespace
#endif
