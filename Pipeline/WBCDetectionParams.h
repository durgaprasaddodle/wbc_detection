#ifndef WBCDETECTIONPARAMS_H
#define WBCDETECTIONPARAMS_H
#include <string>
namespace spin
{
  struct WBCDetectionParams
  {
    std::string white_image_path = "";
    std::string output_file_path = "";
    std::string magnification = "";
    float min_wbc_size = 0;
    float max_wbc_size = 0;
    float foregroundThresh = 0;
    float foregroundThresh40x = 0;
    float foregroundRatio = 0.1;
    float foregroundRatio40x = 0;
    float peak_thresh = 0;
    int min_comp_size = 0;
    int max_comp_size = 0;
    int num_peaks = 0;
    int pallor_threshold = 0;
    int pallor_threshold40x = 0;
    int min_votes = 0;
    double pix2um = 0;
    double pix2um40x = 0;
    int grid_num = 0;
    int hist_bins = 0;
    int debris_size = 0;
    float unsharp_factor  = 0;
    int gkernel_size = 0;
    int bg_blob_area =0;
    float background_ratio  = 0;
    int min_cp_size = 0;
    int max_cp_size = 0;
    int run_detection = 0;
    float min_wbc_area =  0;
    float ecc_thresh = 0;
    float solidity_thresh = 0;
    std::string debug_path = "";
  };
}//end of namespace
#endif
