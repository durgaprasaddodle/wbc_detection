#include "WBCDetectionParser.h"
#include "WBCDetectionParams.h"
//boost includes
#include "boost/filesystem.hpp"
#include "boost/property_tree/xml_parser.hpp"
#include "boost/property_tree/ptree.hpp"
#include "boost/foreach.hpp"

//system includes
#include <iostream>

namespace fs = boost::filesystem;

namespace spin
{
  /*! \brief Default constructor */
  WBCDetectionParser::WBCDetectionParser()
  {
    mWBCParams = std::make_shared<WBCDetectionParams>();
  }//end of function

  /* \! Method to read the input xml file
   *  for parameters
   */
  int WBCDetectionParser::ParseParams(const char * inpFilePath)
  {
    // The parameter file will be in this path
    boost::filesystem::path basepath(inpFilePath);
    std::string fPath = basepath.parent_path().string();
    std::ifstream input(inpFilePath);
    // populate tree structure pt
    using boost::property_tree::ptree;
    ptree pt;
    try
    {
      // parse the xml file
      read_xml(input, pt);
    }
    catch (const boost::property_tree::ptree_error &e)
    {
      return -1;
    }

    BOOST_FOREACH(ptree::value_type& v_haemo, pt.get_child("WBCDetectionParameters"))
    {
      if (v_haemo.first == "General")
      {
        //Parse the Generic Parameters
        BOOST_FOREACH(ptree::value_type& v1, v_haemo.second)
        {
          if (v1.first == "WhiteImagePath")
            mWBCParams.get()->white_image_path = v1.second.get<std::string>("");
          else
          if (v1.first == "OutputFilePath")
            mWBCParams->output_file_path = v1.second.get<std::string>("");
          else
          if (v1.first == "PixelToMicrometer")
            mWBCParams->pix2um = v1.second.get<float>("");
          else
          if (v1.first == "PixelToMicrometer40x")
            mWBCParams->pix2um40x = v1.second.get<float>("");
          else
          if (v1.first == "MinWBCSize")
            mWBCParams->min_wbc_size = v1.second.get<float>("");
          else
          if (v1.first == "MaxWBCSize")
            mWBCParams->max_wbc_size = v1.second.get<float>("");
          else
          if (v1.first == "ForegroundThreshold")
            mWBCParams->foregroundThresh = v1.second.get<float>("");
          else
          if (v1.first == "ForegroundThreshold40x")
            mWBCParams->foregroundThresh40x = v1.second.get<float>("");
          else
          if (v1.first == "ForegroundRatio")
            mWBCParams->foregroundRatio = v1.second.get<float>("");
          else
          if (v1.first == "ForegroundRatio40x")
            mWBCParams->foregroundRatio40x = v1.second.get<float>("");
          else
          if (v1.first == "NumberOfPeaks")
            mWBCParams->num_peaks = v1.second.get<float>("");
          else
          if (v1.first == "PeakThreshold")
            mWBCParams->peak_thresh = v1.second.get<float>("");
          else
          if (v1.first == "MinimumVotes")
            mWBCParams->min_votes = v1.second.get<float>("");
          else
          if (v1.first == "NumberOfPallors")
            mWBCParams->pallor_threshold = v1.second.get<float>("");
          else
          if (v1.first == "NumberOfPallors40x")
            mWBCParams->pallor_threshold40x = v1.second.get<float>("");
          else
          if (v1.first == "MinComponentSize")
            mWBCParams->min_comp_size = v1.second.get<float>("");
          else
          if (v1.first == "MaxComponentSize")
            mWBCParams->max_comp_size = v1.second.get<float>("");
          else
          if (v1.first == "GridSize")
            mWBCParams->grid_num = v1.second.get<float>("");
          else
          if (v1.first == "HistBins")
            mWBCParams->hist_bins = v1.second.get<float>("");
          else
          if (v1.first == "MinCPSize")
			mWBCParams->min_cp_size = v1.second.get<float>("");
          else
          if (v1.first == "MaxCPSize")
            mWBCParams->max_cp_size = v1.second.get<float>("");
          else
          if (v1.first == "BackgroundRatio")
            mWBCParams->background_ratio = v1.second.get<float>("");
          else
          if (v1.first == "GaussianKernel")
            mWBCParams->gkernel_size = v1.second.get<int>("");
          else
          if (v1.first == "UnsharpFactor")
            mWBCParams->unsharp_factor = v1.second.get<float>("");
          else
          if (v1.first == "BGBlobArea")
            mWBCParams->bg_blob_area = v1.second.get<float>("");
          else
          if (v1.first == "RunDetection")
            mWBCParams->run_detection = v1.second.get<int>("");
          else
          if (v1.first == "EccentricityThresh")
            mWBCParams->ecc_thresh = v1.second.get<float>("");
          else
          if (v1.first == "MinWBCArea")
            mWBCParams->min_wbc_area = v1.second.get<float>("");
		  if (v1.first == "SolidityThresh")
			  mWBCParams->solidity_thresh = v1.second.get<float>("");
          if (v1.first == "DebugPath")
              mWBCParams->debug_path = v1.second.get<std::string>("");


        }//end foreach
      }
    }

    return 1;
  }//end of function

  /* \! Method to Display Params
   *
   */
  int WBCDetectionParser::DisplayParams()
  {
    std::cout << " --------- Pipeline configuration --------- " << std::endl;
    std::cout << " === WBC Detection Parameters === " << std::endl;
    std::cout << " White Image Path : " << mWBCParams->white_image_path << std::endl;
    std::cout << " Output File path : " << mWBCParams->output_file_path << std::endl;
    std::cout << " Pixel To Micrometer : " << mWBCParams->pix2um << std::endl;
    std::cout << " Minimum WBC Size : " << mWBCParams->min_wbc_size << std::endl;
    std::cout << " Maximum WBC Size : " << mWBCParams->max_wbc_size << std::endl;
    std::cout << " Histogram Bins : " << mWBCParams->hist_bins << std::endl;
    std::cout << " Number of Grids : " << mWBCParams->grid_num<<std::endl;
    std::cout << " Pixel To Micrometer 40x" <<mWBCParams->pix2um40x<<std::endl;
    std::cout << " Foreground Threshold "<<mWBCParams->foregroundThresh<<std::endl;
    std::cout << " Foreground Ratio " << mWBCParams->foregroundRatio<<std::endl;
    std::cout << " Kernel Size : "<< mWBCParams->gkernel_size<<std::endl;
    std::cout << " Unsharp Factor : "<<mWBCParams->unsharp_factor<<std::endl;
    std::cout << " DebugPath : "<<mWBCParams->debug_path<<std::endl;

    return 0;
  }

  std::string WBCDetectionParser::GetWhitePath()
  {
    return mWBCParams->white_image_path;
  }

  std::string WBCDetectionParser::GetDebugPath()
  {
    return mWBCParams->debug_path;
  }

  std::string WBCDetectionParser::GetOutputFilePath()
  {
    return mWBCParams->output_file_path;
  }

  float WBCDetectionParser::GetSolidityThresh()
  {
    return mWBCParams->solidity_thresh;
  }

  float WBCDetectionParser::GetMinWBCSize()
  {
    return mWBCParams->min_wbc_size;
  }

  float WBCDetectionParser::GetMaxWBCSize()
  {
    return mWBCParams->max_wbc_size;
  }

  double WBCDetectionParser::GetForegroundThresh()
  {
    return mWBCParams->foregroundThresh;
  }

  double WBCDetectionParser::GetForegroundThresh40x()
  {
      return mWBCParams->foregroundThresh40x;
  }

  double WBCDetectionParser::GetPix2UMFactor()
  {
    return mWBCParams->pix2um;
  }

  double WBCDetectionParser::GetPix2UMFactor40x()
  {
      return mWBCParams->pix2um40x;
  }

  float WBCDetectionParser::GetMinVotes()
  {
    return mWBCParams->min_votes;
  }

  float WBCDetectionParser::GetForegroundRatio()
  {
    return mWBCParams->foregroundRatio;
  }

  float WBCDetectionParser::GetForegroundRatio40x()
  {
      return mWBCParams->foregroundRatio40x;
  }

  float WBCDetectionParser::GetMaxComponenSize()
  {
    return mWBCParams->max_comp_size;
  }

  float WBCDetectionParser::GetMinComponenSize()
  {
    return mWBCParams->min_comp_size;
  }

  float WBCDetectionParser::GetNumberOfPeaks()
  {
    return mWBCParams->num_peaks;
  }

  float WBCDetectionParser::GetPeakThresh()
  {
    return mWBCParams->peak_thresh;
  }

  float WBCDetectionParser::GetPallorNumber()
  {
    return mWBCParams->pallor_threshold;
  }

  int WBCDetectionParser::GetHistBins()
  {
    return mWBCParams->hist_bins;
  }

  float WBCDetectionParser::GetPallorNumber40x()
  {
      return mWBCParams->pallor_threshold40x;
  }

  int WBCDetectionParser::GetNumberofGrids()
  {
    return mWBCParams->grid_num;
  }

  int WBCDetectionParser::GetDerisSize()
  {
      return mWBCParams->debris_size;
  }

  int WBCDetectionParser::GetKernelSize()
  {
      return mWBCParams->gkernel_size;
  }

  float WBCDetectionParser::GetUnsharpFactor()
  {
      return mWBCParams->unsharp_factor;
  }

  float WBCDetectionParser::GetMinWBCArea()
  {
      return mWBCParams->min_wbc_area;
  }

  float WBCDetectionParser::GetEccThresh()
  {
      return mWBCParams->ecc_thresh;
  }

  int WBCDetectionParser::GetBGBlobArea()
  {
    return mWBCParams->bg_blob_area;
  }

  float WBCDetectionParser::GetBackgroundRatio()
  {
    return mWBCParams->background_ratio;
  }

  int WBCDetectionParser::GetMinCPSize()
  {
    return mWBCParams->min_cp_size;
  }

  int WBCDetectionParser::GetMaxCPSize()
  {
    return mWBCParams->max_cp_size;
  }

}//end of namespace
