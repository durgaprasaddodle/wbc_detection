#include <tuple>
#include <string>
#include <map>

// Common headers to be used by plugin and unit-test
#include "WBCDetectionParser.h"
#include "WBCStruct.h"
//opencv headers
#include "opencv2/opencv.hpp"
//boost headers
#include "boost/timer/timer.hpp"
#include "boost/property_tree/json_parser.hpp"
#include "boost/filesystem.hpp"
//armadillo headers
#include <armadillo>


using namespace arma;
namespace fs = boost::filesystem;
namespace spin
{
  template<class T>
  class WBCDetection
  {
    public:
      // Default constructor
      WBCDetection(){}
      // Default destructor
      ~WBCDetection()
      {
        allAOI.clear();
      }

      /*! \brief InstantiatePipeline */
      int InstantiatePipeline(const char* inp);

      /*! \brief Process Pipeline */
      int ProcessPipeline(void* input, std::string n);

      /*! \brief Complete Processing */
      int CompleteProcessing();


    private:
      /*! \brief Check Region at 20x*/
      int CheckRegion(void* _input, void* _output);

      /*! \brief CheckRegionHighMag at 40x*/
      int CheckRegionHighMag(void* _input, void* _output);

      /*! \brief Background Detection */
      int BackgroundDetection(void* _input, void* _output);

      /*! \brief Detect cell */
      int DetectCell(void* _input, void* _output, int mag);

      /*! \brief MaxEntropy Thresholding in OpenCV */
      uchar MaxEntropyThreshold(const cv::Mat1b& src, cv::Mat1b& dst);

      /*!  \brief A datastructure holding information about WBC's */
      std::vector<std::pair<int, WBCStruct>> allAoiInfo;

      /*! \brief A data structure holding information about WBC's */
      std::map<std::string, WBCStruct> allAOI;

      /*! \brief A parameter parser */
      std::shared_ptr<WBCDetectionParser> mParams;

      /*! \brief A calibration image */
      T white_image;
\
  };//end of class
}
