﻿#include<vector>
#include<iostream>

#include "FRST.h"
#include "TouchingCellSegmentation.h"


int TouchingCellSegmentation::ComputeFRST(const cv::Mat &inp, cv::Mat &out)
{
// apply FRST
  cv::Mat frstImage;

  //frst2d(inp, frstImage, 36,16, 1, FRST_MODE_BRIGHT);
  frst2d(inp, frstImage, 10,5, 1, FRST_MODE_BRIGHT);

  // the frst will have irregular values, normalize them!
  cv::normalize(frstImage, frstImage, 0.0, 1.0, cv::NORM_MINMAX);
  frstImage.convertTo(frstImage, CV_8U, 255.0);

  out = frstImage;
  return 0;
}

int TouchingCellSegmentation::Watershed(cv::Mat &img, const cv::Mat &mask,
                                      std::vector<std::vector<cv::Point>> cont,
                                      cv::Mat &out)
{
  std::vector<std::vector<cv::Point>> distCont;
  cv::findContours(mask, distCont, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);

  cv::Mat markers1 = cv::Mat::zeros(mask.size(), CV_32SC1);

  for (size_t i = 0; i < distCont.size(); i++)
  {
    cv::drawContours(markers1, distCont, static_cast<int>(i),
               cv::Scalar::all(static_cast<int>(i)+1), -1);
  }
  cv::circle(markers1, cv::Point(5,5), 3, cv::Scalar::all(255), -1);

  cv::watershed(img, markers1);
  cv::Mat mark = cv::Mat::zeros(markers1.size(), CV_8UC1);

  markers1.convertTo(mark, CV_8UC1);
  cv::bitwise_not(mark, mark);

  std::vector<cv::Vec3b> colors;
  for (size_t i = 0; i < distCont.size(); i++)
  {
    int b = cv::theRNG().uniform(0, 255);
    int g = cv::theRNG().uniform(0, 255);
    int r = cv::theRNG().uniform(0, 255);
    colors.push_back(cv::Vec3b((uchar)b, (uchar)g, (uchar)r));
  }

  // Create the result image
  cv::Mat dst = cv::Mat::zeros(markers1.size(), CV_8UC3);

 for (int i = 0; i < markers1.rows; i++)
  {
    for (int j = 0; j < markers1.cols; j++)
    {
      int index = markers1.at<int>(i,j);
      if (index > 0 && index <= static_cast<int>(distCont.size()))
          dst.at<cv::Vec3b>(i,j) = colors[index-1];
      else
          dst.at<cv::Vec3b>(i,j) = cv::Vec3b(0,0,0);

      if(index == -1)
      {
          img.at<cv::Vec3b>(i,j) = cv::Vec3b(0,0,0);
      }
    }
  }
  out = dst;
  return 0;
}

int TouchingCellSegmentation::ComputeDistTransform(const cv::Mat &img,
                                                const cv::Mat& inp,
                                                std::vector<cv::Point> contours,
                                                cv::Mat &out
                                                )
{
  cv::Mat waterOut;
  cv::Mat dist, dist_norm, segImg;
  cv::Mat dist_8u, maskUpdated, frstInput;
  std::vector<std::vector<cv::Point>> t_cont;

  maskUpdated = inp.clone();
  cv::Mat mask = inp.clone();
  //t_cont.push_back(contours);

//  cv::Mat mask = cv::Mat::zeros(inp.rows, inp.cols, CV_8UC1);

//  cv::drawContours(mask, t_cont, 0, cv::Scalar(255), cv::FILLED);

  cv::distanceTransform(mask, dist, cv::DIST_L2, 3);
  cv::normalize(dist, dist_norm, 0, 1., cv::NORM_MINMAX);

  frstInput = (dist_norm * 240);
  frstInput.convertTo(frstInput, CV_8U);

  dist_norm.convertTo(dist_8u, CV_8U);

  cv::cvtColor(maskUpdated, maskUpdated, cv::COLOR_GRAY2BGR);
  cv::bitwise_and(img, maskUpdated, segImg);

  cv::Mat temp;
  ComputeFRST(frstInput, temp);

  cv::cvtColor(mask, mask, cv::COLOR_GRAY2BGR);

  Watershed(segImg, temp, t_cont, waterOut);

//  cv::waitKey(0);
  cv::Mat border = cv::Mat::zeros(img.rows, img.cols, CV_8UC3);
  border.setTo(cv::Scalar::all(255), waterOut == 0);
  cv::bitwise_and(border, mask, border);
  cv::dilate(border, border, cv::getStructuringElement(cv::MORPH_ELLIPSE,
                                                        cv::Size(3, 3)),
            cv::Point(-1,-1));

  cv::Mat mask_final;
  waterOut.setTo(cv::Scalar::all(0), border > 0);
  cv::bitwise_and(mask, waterOut, mask_final);

  out = mask_final;
  t_cont.clear();
  return 0;
}

int TouchingCellSegmentation::SegmentTouchingCells(const cv::Mat &img,
                                                 cv::Mat &mask,
                                                 std::vector<cv::Point> contour,
                                                 cv::Mat &out
                                                 )
{
  ComputeDistTransform(img, mask, contour, out);
  return 0;
}
