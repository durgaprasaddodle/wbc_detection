#include "WBCDetection.h"
#include "TouchingCellSegmentation.h"

using namespace arma;
namespace fs = boost::filesystem;
namespace spin
{

  /*! \brief InstantiatePipeline
   *  This function expectes the presence of a parameter file and parses
   *  it. This loads up all auxiliary data necessary to execute this plugin
   *  on input data
   *  @inp        : The input parameter file that needs to be parsed
   *  @obj		  : The global object hold additional information
   */
  template<class T>
  int WBCDetection<T>::InstantiatePipeline(const char* inp)
  {
    mParams = std::make_shared<WBCDetectionParser>();
    //read parameter file
    int v = mParams->ParseParams(inp);
    if (v != 1) return -1;
    mParams->DisplayParams();
    //read white image TODO: Uncomment lines below
    white_image = cv::imread(mParams->GetWhitePath());

    if (white_image.empty()) return -2;

    return 1;
  }//end of function


  /*! \brief ProcessPipeline
   *  This function is a driver to run the wbc detection pipeline
   *  @input      : The producer data structure that contains all information
   *                needed to run the detection pipeline
   *  @aoiInfo    : Any auxiliary information. For now, we ignore this variable
   *  @output     : Is there any output generated from this pipeline?
   */
  template<class T>
  int WBCDetection<T>::ProcessPipeline(void* _input, std::string n)
  {

    // Make a copy of the input image here
    cv::Mat* dataInput = static_cast<cv::Mat*>(_input);

    // Create a WBCStruct object
    WBCStruct wObj;
    wObj.setAoiName(n);;
    // 1.) Run Monolayer detection
    int reg_out;
    reg_out = this->CheckRegion(dataInput, &wObj);
    this->DetectCell(dataInput, &wObj, 1);
    wObj.setAoiName(n);

    allAOI.insert(std::pair<std::string, WBCStruct>(n, wObj));

    // Free the image
    //dataInput.release();
    return reg_out;
    //end of function
  }


  /*! \brief BackgroundDetection
   *  This function......(describe it here)
   */
  template<class T>
  int WBCDetection<T>::BackgroundDetection(void* _input, void* _output)
  {
    return 1;
  }//end of function

  /*! \brief Checkregion
   *  This function detects whether the fov is
   *  monolayer, thin, thick or very thin
   *  Divide the image into 5x5 grid and classify each grid
   *  based on ratio of area of non overlapping cells to total
   *  cells and counting number of center pallors present in the image
   *  @ _input :- cv::Mat image
   *  @ _output :- WBCStruct which gets updated and returned to ProcessPipeline
   */
  template<class T>
  int WBCDetection<T>::CheckRegion(void* _input, void* _output)
  {

    WBCStruct* mObj = static_cast<WBCStruct*>(_output);


    //Retrieve data from voi pointers
    //std::cout<<white_image.size();
    //return 1;
    cv::Mat* inpLeftImage = static_cast<cv::Mat*>(_input);
//    cv::Mat left_white_corr = 200*((*inpLeftImage)/(white_image));
    //changed white correction
    cv::Mat left_white_corr = (*inpLeftImage);

//    cv::Mat copyimg = left_white_corr.clone() ;
    cv::Mat saveimg = left_white_corr.clone() ;

    std::vector<cv::Mat> rgb_split;
    cv::split(left_white_corr, rgb_split);

//    cv::namedWindow( "ORIGINAL",cv::WINDOW_NORMAL );
//    cv::resizeWindow("ORIGINAL", 600, 600);
//    cv::imshow("ORIGINAL",*inpLeftImage);
//    cv::waitKey(0);

    //This part checks is aoi is sparse or not
    //Segment out the foreground and take the ratio of
    //total foreground area to total image area. If its less than
    //a threshold then its sparse
    cv::Mat bw_mask;
    cv::threshold(rgb_split[1], bw_mask, 0, 255, cv::THRESH_OTSU | cv::THRESH_BINARY_INV);

    //release vector
    //rgb_split.clear();
    //1bw_mask.release();
    //left_white_corr.release();


    //we gonna clean the hell out of this image.
    cv::Mat se1 = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3, 3), cv::Point(-1, -1));
    //median filtering
    cv::medianBlur(bw_mask, bw_mask, 3);
    cv::morphologyEx(bw_mask, bw_mask, cv::MORPH_OPEN, se1, cv::Point(-1, -1));
    cv::morphologyEx(bw_mask, bw_mask, cv::MORPH_CLOSE, se1, cv::Point(-1, -1));

    //find contours
    std::vector<std::vector<cv::Point>> contoursGreen;
    //std::vector<cv::Vec4i> hierarchyGreen;

    findContours(bw_mask, contoursGreen, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE, cv::Point(0, 0));
    cv::Mat binary_mask = cv::Mat::zeros(bw_mask.size(), CV_8UC1);
    cv::Mat binary_filled = cv::Mat::zeros(bw_mask.size(), CV_8UC1);
    //To account for noise and debris, just remove some components
    binary_mask = bw_mask.clone();
    double cells_area =0 ;
    //#pragma omp parallel for
    for (int i = 1; i < contoursGreen.size(); i++)
    {
      if (contoursGreen[i].size() < 5)
        continue;
      else
      {
        cv::RotatedRect ell = cv::fitEllipse(contoursGreen[i]);
        float maj_axis = std::max(ell.size.width, ell.size.height)*mParams.get()->GetPix2UMFactor();
        if (maj_axis > 3 && maj_axis < 10)
            cells_area += cv::contourArea(contoursGreen[i]);
        if (maj_axis > mParams.get()->GetDerisSize())
        {
          cv::drawContours(binary_mask, contoursGreen, i, cv::Scalar(255), cv::FILLED);

          //cv::elMaxEntropyThresholdlipse((*left_image), ell, cv::Scalar(0, 100, 255));
        }

      }
    }
    //contoursGreen.clear();

    fs::path very_thin_debug_path(mParams->GetDebugPath() + "/very_thin");
    if (!fs::is_directory(very_thin_debug_path))
        fs::create_directory(very_thin_debug_path);

    fs::path thin_debug_path(mParams->GetDebugPath() + "/thin_region");
    if (!fs::is_directory(thin_debug_path))
        fs::create_directory(thin_debug_path);

    fs::path mono_debug_path(mParams->GetDebugPath() + "/monolayer");
    if (!fs::is_directory(mono_debug_path))
        fs::create_directory(mono_debug_path);

    fs::path thick_debug_path(mParams->GetDebugPath() + "/thick_region");
    if (!fs::is_directory(thick_debug_path))
        fs::create_directory(thick_debug_path);


    //Check the foreground percentage.
    cv::Mat foregroundPix;
    cv::findNonZero(binary_mask, foregroundPix);
    double blobsAndFGRatio = (cells_area/(foregroundPix.rows)) ;
    double foregroundRatio = double(foregroundPix.rows) / double(binary_mask.rows*binary_mask.cols);
    std::cout<<foregroundRatio<<" "<<blobsAndFGRatio<<std::endl;
    if (foregroundRatio < mParams.get()->GetForegroundThresh() && blobsAndFGRatio < 0.3 )
    {
        //1 state if it has more background than foreground
        //that means very thin region
        std::string region_tag = "VeryThin";
        //std::string out_path = "/home/adminspin/Documents/PB/Capture1/acq_ws_Slide345/grid_1/new/verythin/" + mObj->GetAoiName() + ".png";
        std::string out_path = very_thin_debug_path.string() + "/" + mObj->GetAoiName() + ".png";
        cv::imwrite(out_path, saveimg);
        std::cout<<"Verythin      "+mObj->GetAoiName()+ "   ForeGroundRatio   "<<foregroundRatio<<std::endl;
        mObj->setTotalFG(foregroundRatio);
        mObj->setRegionTag(region_tag);
        //mObj->setFGVal(foregroundRatio);
        left_white_corr.release();
        binary_mask.release();
        contoursGreen.clear();
        se1.release();
        binary_filled.release();
        bw_mask.release();
        //allAOI.insert(std::pair<std::string, WBCStruct>(n, mObj));
        //ofs.close();
        return -1;
    }

    //convert to lab;
    cv::Mat lab_image;
    cvtColor(left_white_corr, lab_image, cv::COLOR_BGR2Lab);
    lab_image.convertTo(lab_image, CV_8UC3);

    //split lab image
    std::vector<cv::Mat> lab_ch;
    cv::split(lab_image, lab_ch);



    //try some unsharp masking
    int gauss_size = mParams.get()->GetKernelSize();
    cv::Mat l_blur, l_unsharp;
    cv::GaussianBlur(lab_ch[0].clone(), l_blur, cv::Size(gauss_size, gauss_size), double(gauss_size) / 6);
    cv::Mat l = lab_ch[0];
    l.convertTo(l, CV_32FC1);
    l_blur.convertTo(l_blur, CV_32FC1);
    l_unsharp = l - mParams.get()->GetUnsharpFactor()*l_blur;
    l_unsharp = l + l_unsharp;
    l_unsharp.convertTo(l_unsharp, CV_8UC1);
    l_blur.release(); l.release();
    //threshold using otsu
    cv::Mat bw_l;
    //l_unsharp = lab_ch[0].clone();
    //lab_ch.clear();

    cv::Mat bw_cp;
    cv::threshold(l_unsharp, bw_l, 0, 255, cv::THRESH_OTSU | cv::THRESH_BINARY);
    cv::threshold(lab_ch[0], bw_cp, 0, 255, cv::THRESH_OTSU | cv::THRESH_BINARY);
    cv::medianBlur(bw_l, bw_l, 5);
    cv::Mat bw_inv = 255 - bw_l;

    cv::Mat bw_mask1;
    //to check if its a verythin region or not
    cv::threshold(lab_ch[0], bw_mask1, 0, 255, cv::THRESH_BINARY_INV | cv::THRESH_OTSU);
    //median blur
    cv::medianBlur(bw_mask1, bw_mask1, 5);
    //some erosion
//    cv::imshow("binary_filled", bw_mask1);
//    cv::imshow("image", left_white_corr);
//    cv::waitKey(0);
    cv::Mat binaryimg = bw_mask1.clone();

    cv::Mat se_new = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3, 3), cv::Point(-1, -1));
    cv::morphologyEx(bw_mask1, bw_mask1, cv::MORPH_DILATE, se_new, cv::Point(-1, -1), 1);

    //detect blob
    //findContours(bw_mask, contoursGreen, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE, cv::Point(0, 0));

    cv::Mat biggest_cont;
    biggest_cont = bw_mask1.clone();


    //invert image to get background
    biggest_cont = 255 - binary_mask.clone();

    //erode heavily  so that small blobs disappear and only big background areas should remain
    se1 = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(9, 9), cv::Point(-1, -1));
    //cv::imshow("biggest Cont",biggest_cont);
    cv::erode(biggest_cont, biggest_cont, se1, cv::Point(-1, -1), 3);

    //find contours again
    findContours(biggest_cont.clone(), contoursGreen, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE, cv::Point(0, 0));

    //check what percentage of total background is biggerblobs
    int white_pix = 0;
    //white_pix = cv::countNonZero(biggest_cont);
    double bg_area = 0;

//    cv::Mat temp_out = cv::Mat::zeros(biggest_cont.size(), CV_8UC1);
    for (int i = 0; i < contoursGreen.size(); i++)
    {
      white_pix +=  cv::contourArea(contoursGreen[i]);
      if (cv::contourArea(contoursGreen[i]) > 3000)
      {
        bg_area += cv::contourArea(contoursGreen[i]);
//        cv::drawContours(temp_out, contoursGreen, i, cv::Scalar::all(255), cv::FILLED);
      }
    }

//    imshow("ads", temp_out);
//    imshow("ads2", biggest_cont);
//    cv::waitKey(0);
    bg_area = bg_area / double(white_pix);
    //std::cout<<mObj->GetAoiName()<<"          Bg Ratio at second check    :"<<bg_area<<std::endl;
    //If foreground has bigger blobs then its near the tail
    std::cout<<bg_area<<" "<<blobsAndFGRatio<<std::endl;
    if (bg_area > 0.8 && blobsAndFGRatio < 0.3 ) //should be 0.6
    {
      //std::string out_path = "/home/adminspin/Documents/PB/Capture1/acq_ws_Slide345/grid_1/new/verythin/" + mObj->GetAoiName() + ".png";
      std::string out_path = very_thin_debug_path.string() + "/" + mObj->GetAoiName() + ".png";

      cv::imwrite(out_path, saveimg);
      std::cout<<"Verythin      "+mObj->GetAoiName()+ "   BackgroundArea   "<<bg_area<<std::endl;

      std::string region_tag = "VeryThin";
      mObj->setTotalBG(bg_area);
      mObj->setRegionTag(region_tag);
      return -1;

    }


    std::vector<std::vector<cv::Point>> contours_bw;
    std::vector<cv::Vec4i> hierarchy_bw;
    cv::findContours(bw_mask1, contours_bw, hierarchy_bw,cv::RETR_TREE, cv::CHAIN_APPROX_NONE, cv::Point(0, 0));

    binary_filled = bw_mask1.clone();
    //#pragma omp parallel for
    for (int i = 0; i < contours_bw.size(); i++)
    {
      if (contours_bw[i].size() < 5)
        continue;
      else
      {
        cv::RotatedRect ell = cv::fitEllipse(contours_bw[i]);
        float maj_axis = std::max(ell.size.width, ell.size.height)*mParams.get()->GetPix2UMFactor();

        if(hierarchy_bw[i][3] !=-1)
        {
        if (maj_axis > 1 && maj_axis < 8)
        {

          cv::drawContours(binary_filled, contours_bw, i, cv::Scalar(255), cv::FILLED);
          //cv::ellipse((*left_image), ell, cv::Scalar(0, 100, 255));
        }
        }
      }
    }

    cv::Mat out;
    TouchingCellSegmentation obj;
    obj.SegmentTouchingCells(left_white_corr, binary_filled, std::vector<cv::Point>(), out);
//    cv::imshow("out",binary_filled);

    cv::cvtColor(out, out, cv::COLOR_BGR2GRAY);
    binary_filled = out>0;

    cv::findContours(binary_filled, contours_bw, hierarchy_bw,cv::RETR_TREE, cv::CHAIN_APPROX_NONE, cv::Point(0, 0));

    //#pragma omp parallel for
    for (int i = 0; i < contours_bw.size(); i++)
    {
      if (contours_bw[i].size() < 5)
        continue;
      else
      {
        cv::RotatedRect ell = cv::fitEllipse(contours_bw[i]);
        float maj_axis = std::max(ell.size.width, ell.size.height)*mParams.get()->GetPix2UMFactor();

        if(hierarchy_bw[i][3] !=-1)
        {
        if (maj_axis > 1 && maj_axis < 8)
        {

          cv::drawContours(binary_filled, contours_bw, i, cv::Scalar(255), cv::FILLED);
          //cv::ellipse((*left_image), ell, cv::Scalar(0, 100, 255));
        }
        }
      }
    }

    //find center pallors. bw_l will now have center pallors

      bw_l = bw_cp.clone();
      cv::multiply(bw_l, binary_filled, bw_l);
      cv::findContours(bw_l, contours_bw, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE, cv::Point(0, 0));

    // #pragma omp parallel for
  std::vector<float> axisarray;
    for (int i = 0; i < contours_bw.size(); i++)
    {
      //remove components less than 5 pixel size becasue
      //ellipse fitting will fail
      if (contours_bw[i].size() <= 5)
        cv::drawContours(bw_l, contours_bw, i, cv::Scalar::all(0), cv::FILLED);
      else
      {
        //again remove very big blobs found because center pallors
        //cannot be that big
        cv::RotatedRect rect = cv::fitEllipse(contours_bw[i]);
        double MajA = std::max(rect.size.width, rect.size.height);
        MajA = MajA*mParams.get()->GetPix2UMFactor();
        axisarray.push_back(MajA);
        if (MajA > 5 || MajA < 1)
        {
          cv::drawContours(bw_l, contours_bw, i, cv::Scalar::all(0), cv::FILLED);
        }
        else
        {
          cv::drawContours(left_white_corr, contours_bw, i, cv::Scalar::all(255), cv::FILLED);
        }

      }
    }


    cv::Mat se_erode1 = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5), cv::Point(-1, -1));
    //cv::erode(binary_filled, binary_filled, se_erode1, cv::Point(-1,-1), 2);

    contours_bw.clear();

    //get 100x grids
    std::vector<cv::Rect> grids;
    //this is to divide image into RxR grids.
    //if you dont want to keep the grids constant in x and y
    //direction i.e if you want to generate RxN grids, then revisit this part
    int number_of_grids = mParams.get()->GetNumberofGrids();

    //get grid width
    int grid_width = left_white_corr.cols / number_of_grids;
    int grid_height = left_white_corr.rows / number_of_grids;
    int grid_id = 0;
    int vote = 0;
    double fg_ratio = 0;
    std::vector<point> mono_points;

    //This is where we detect mono,thin, thick.
    //Estimate the ratio of area of total foreground to total non-overlapping
    //cells. If this ratio is bigger than a certain threshold then it should be
    //thin or mono. To detect monolayer, in every grid, find out the number of center
    //pallors present. If it excedes a certain threshold, then that grid belongs to monolayer.
    //Check parameter file for these parameters.


    for (int i = 0; i < left_white_corr.rows; i = i + grid_height)
    {

      for (int j = 0; j < left_white_corr.cols; j = j + grid_width)
      {
        double total_fore_area = 0;
        double total_cell_area = 0;
        cv::Rect bbox;
        bbox.x = j;
        bbox.y = i;
        //grid_id++;

        //make sure grid is small enough to fit in the image
        if (grid_height > left_white_corr.rows - i - 1)
          continue;
        if (grid_width > left_white_corr.cols - j - 1)
          continue;

        //some boundary conditions
        bbox.height = std::min(grid_height, left_white_corr.rows - i - 1);
        bbox.width = std::min(grid_width, left_white_corr.cols - j - 1);

        //Dont consider the boundary grids. Only central grids are analysed
        //If you change the magnification, number of grids and usable grids(due to
        //focus issues) will change. You may need to revisit this part then.
        if (bbox.x == 0 || bbox.y == 0)
        {
          continue;
        }
        if ((bbox.x + bbox.width >= left_white_corr.cols - 1) || \
        (bbox.y + bbox.height >= left_white_corr.rows - 1))
          continue;
        grid_id++;
        //crop the grid to calcualte individual blobs and center pallors
        cv::Mat bw_crop = bw_l(bbox);  //this is center pallor grid
        cv::Mat bw_crop_filled = binary_filled(bbox); //this is filled cells grid



        //some cleaning
        cv::Mat se_erode = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5), cv::Point(-1, -1));
        cv::erode(bw_crop_filled, bw_crop_filled, se_erode, cv::Point(-1, -1), 1);
        cv::Mat se_erode2 = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5), cv::Point(-1, -1));
        cv::erode(bw_crop_filled, bw_crop_filled, se_erode2, cv::Point(-1, -1), 1);
        se_erode2 = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3, 3), cv::Point(-1, -1));
        cv::dilate(bw_crop_filled, bw_crop_filled, se_erode2, cv::Point(-1, -1), 1);



        //Again findcontours
        cv::findContours(bw_crop_filled.clone(), contours_bw, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE, cv::Point(0, 0));
        total_fore_area = cv::countNonZero(bw_crop_filled);

        float total_cell_count = 0;
        float cp_count = 0;
        cv::Mat temp2 = cv::Mat::zeros(bw_crop_filled.size(), CV_8UC3);
        for (int k = 0; k < contours_bw.size(); k++)
        {
          if (contours_bw[k].size() <= 5)
            continue;
          else
          {
//            //total_fore_area += cv::contourArea(contours_bw[k]);
            cv::RotatedRect rect_ell = cv::fitEllipse(contours_bw[k]);
            double majAx = std::max(rect_ell.size.width, rect_ell.size.height);
            majAx = majAx*mParams.get()->GetPix2UMFactor();
            double area = cv::contourArea(contours_bw[k]);
            double rad = sqrt(area/3.14);
//            majAx = 2*rad*mParams->GetPix2UMFactor();
            double min_area = std::pow(mParams.get()->GetMinComponenSize()/2,2)*3.14;
            area = area*pow(mParams->GetPix2UMFactor(),2);

//            cv::Mat oneBlob = cv::Mat::zeros(bw_crop_filled.size(), CV_8UC1);
//            cout<<"majAx:::  "<<majAx<<endl;
//            cv::drawContours(oneBlob, contours_bw, k, cv::Scalar::all(255), cv::FILLED);

            if (majAx > mParams.get()->GetMinComponenSize() && majAx < mParams.get()->GetMaxComponenSize() &&  area > min_area)
            {
//              cv::drawContours(temp2, contours_bw, k, cv::Scalar::all(255), cv::FILLED);
//              cv::ellipse(temp2, rect_ell, cv::Scalar(0,255,255));
//              cv::imshow("oneCont", temp2);
//              cv::waitKey(0);
              total_cell_area += cv::contourArea(contours_bw[k]);
              total_cell_count++;
              if(bw_crop.at<uchar>(rect_ell.center.y, rect_ell.center.x) !=0)
                  cp_count++;
            }
            else
            {
              continue;
            }
          }
        }
        //calcutate the fg_ratio for everygrid and accumlate. We will take average later on
        if (total_fore_area == 0)
          fg_ratio += 0;
        else
          fg_ratio += total_cell_area / total_fore_area;

        //find center pallors
        cv::findContours(bw_crop.clone(), contours_bw, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE, cv::Point(0, 0));
        //if this grid is monolayer, save the centroid of the grid
        //float cp_count = contours_bw.size();


        if (cp_count/total_cell_count > 0.5 && cp_count/total_cell_count <= 1.4 )
        {
          std::cout<<"CP Ratio : "<<cp_count/total_cell_count <<std::endl;
          point mono_cen;
          mono_cen.x = ceil(bbox.x + bbox.width / 2);
          mono_cen.y = ceil(bbox.y + bbox.height / 2);
          mono_points.push_back(mono_cen);
          vote++;
        }
        /*if (contours_bw.size() > mParams.get()->GetPallorNumber())
        {
          point mono_cen;
          mono_cen.x = ceil(bbox.x + bbox.width / 2);
          mono_cen.y = ceil(bbox.y + bbox.height / 2);
          mono_points.push_back(mono_cen);
          vote++;
        }*/
        bw_crop.release();
        bw_crop_filled.release();
      }

    }
    contours_bw.clear();
    //binary_filled.release();
    bw_l.release();
    bw_inv.release();
    bw_mask.release();
    l_unsharp.release();
    //left_white_corr.release();


    mObj->setMonoVotes(vote);
    //take average of foreground in all grids
    fg_ratio = fg_ratio / grid_id;
    std::cout<<mObj->GetAoiName()<<" "<<fg_ratio<<","<<vote<<std::endl;
    //return 1;

    if (vote >= 0 && vote <= 9)
    {
      if (fg_ratio >= mParams.get()->GetForegroundRatio() && vote < mParams.get()->GetMinVotes())
      {
        mObj->setRegionTag("ThinRegion");
        mObj->setFGVal(fg_ratio);
        mObj->setMonoRegion(mono_points);
        std::string out_path = thin_debug_path.string() + "/" + mObj->GetAoiName() + ".png";
        cv::imwrite(out_path, saveimg);
        std::cout<<"ThinRegion    "+mObj->GetAoiName()+ "   "<<vote <<"   "<<fg_ratio<<std::endl;
        return 1;

      }
      else if (fg_ratio >= mParams.get()->GetForegroundRatio() && vote >= mParams.get()->GetMinVotes())
      {
        mObj->setRegionTag("Monolayer");
        mObj->setMonoRegion(mono_points);
        mObj->setFGVal(fg_ratio);
        std::string out_path = mono_debug_path.string()+ "/"  + mObj->GetAoiName() + ".png";
        //std::string out_path = "/home/adminspin/Documents/PB/Capture1/acq_ws_Slide345/grid_1/new/mono/" + mObj->GetAoiName() + ".png";
        cv::imwrite(out_path, saveimg);
        std::cout<<"Monolayer     "+mObj->GetAoiName()+ "   "<<vote <<"   "<<fg_ratio<<std::endl;
        return 2;

      }
      else
      {
        mObj->setFGVal(fg_ratio);
        mObj->setMonoRegion(mono_points);
        mObj->setRegionTag("ThickRegion");
        std::string out_path = thick_debug_path.string() + "/" + mObj->GetAoiName() + ".png";
        //std::string out_path = "/home/adminspin/Documents/PB/Capture1/acq_ws_Slide345/grid_1/new/thick/" + mObj->GetAoiName() + ".png";
        cv::imwrite(out_path, saveimg);
        std::cout<<"ThickRegion   "+mObj->GetAoiName()+ "   "<<vote <<"   "<<fg_ratio<<std::endl;
        return 3;
      }
    }


    return 0;

  }//end of function

  /*
   *
   *
   */
  template<class T>
  int WBCDetection<T>::CheckRegionHighMag(void* _input, void* _output)
  {

    WBCStruct* mObj = static_cast<WBCStruct*>(_output);
    //WBCStruct mObj = (*struct_object);
    //Retrieve data from voi pointers
    cv::Mat* inpLeftImage = static_cast<cv::Mat*>(_input);

    cv::Mat left_white_corr = 200*((*inpLeftImage)/(white_image));
    left_white_corr.convertTo(left_white_corr, CV_8UC3);
    cv::medianBlur(left_white_corr, left_white_corr, 5);

    //split image
    std::vector<cv::Mat> rgb_split;
    cv::split(left_white_corr, rgb_split);

    //This part checks is aoi is sparse or not
    //Segment out the foreground and take the ratio of
    //total foreground area to total image area. If its less than
    //a threshold then its sparse
    cv::Mat bw_mask;
    cv::threshold(rgb_split[1], bw_mask, 0, 255, cv::THRESH_OTSU | cv::THRESH_BINARY_INV);
    //we gonna clean the hell out of this image.
    cv::Mat se1 = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3, 3), cv::Point(-1, -1));
    //median filtering
    cv::medianBlur(bw_mask, bw_mask, 3);
    cv::morphologyEx(bw_mask, bw_mask, cv::MORPH_OPEN, se1, cv::Point(-1, -1));
    cv::morphologyEx(bw_mask, bw_mask, cv::MORPH_CLOSE, se1, cv::Point(-1, -1));

    //find contours
    std::vector<std::vector<cv::Point>> contoursGreen;
    findContours(bw_mask.clone(), contoursGreen, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE, cv::Point(0, 0));
    cv::Mat binary_mask = cv::Mat::zeros(bw_mask.size(), CV_8UC1);
    cv::Mat binary_filled = cv::Mat::zeros(bw_mask.size(), CV_8UC1);

    //To account for noise and debris, just remove some components
    binary_mask = bw_mask.clone();
    for (int i = 0; i < contoursGreen.size(); i++)
    {
      if (contoursGreen[i].size() < 5)
        continue;
      else
      {
        cv::RotatedRect ell = cv::fitEllipse(contoursGreen[i]);
        float maj_axis = std::max(ell.size.width, ell.size.height)*mParams.get()->GetPix2UMFactor40x();

        if (maj_axis < mParams.get()->GetDerisSize())  //parameter to account for debris = 3
        {
          cv::drawContours(binary_mask, contoursGreen, i, cv::Scalar(0), cv::FILLED);
          //cv::ellipse((*left_image), ell, cv::Scalar(0, 100, 255));
        }
      }
    }
    cv::Mat foregroundPix;
    cv::findNonZero(binary_mask, foregroundPix);
    double foregroundRatio = double(foregroundPix.rows) / double(binary_mask.rows*binary_mask.cols);

    if (foregroundRatio <= mParams.get()->GetForegroundThresh40x())  //for 40x its 0.43
    {
      //that means very thin region
      std::string region_tag = "VeryThin";
      mObj->setFGVal(foregroundRatio);
      mObj->setRegionTag(region_tag);
      return -1;
    }

    se1 = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(7, 7), cv::Point(-1, -1));

    //convert to lab;
    cv::Mat lab_image1;
    cvtColor(left_white_corr, lab_image1, cv::COLOR_BGR2Lab);
    lab_image1.convertTo(lab_image1, CV_8UC3);

    cv::imshow("input", left_white_corr);
    cv::waitKey(0);
    std::cout<<"\n\n\nHere \n\n\n"<<std::endl;

    //split lab image
    std::vector<cv::Mat> lab_ch1;
    cv::split(lab_image1, lab_ch1);
    cv::Mat Il = lab_ch1[0];
    cv::medianBlur(Il, Il, 5);

    //perform unsharp masking for edge enhancement
    int gauss_size = mParams.get()->GetKernelSize(); // 25 for 40x
    cv::Mat L_blur, L_unsharp;
    cv::GaussianBlur(Il.clone(), L_blur, cv::Size(gauss_size, gauss_size), gauss_size / 6);
    cv::Mat L = Il.clone();
    L.convertTo(L, CV_32FC1);
    L_blur.convertTo(L_blur, CV_32FC1);
    L_unsharp = L - mParams.get()->GetUnsharpFactor()*L_blur; //1.75 for 40x
    L_unsharp = L + L_unsharp;
    L_unsharp.convertTo(L_unsharp, CV_8UC1);

    //threshold image
    cv::threshold(L_unsharp, bw_mask, 0, 255, cv::THRESH_BINARY_INV | cv::THRESH_OTSU);
    //median blur
    cv::medianBlur(bw_mask, bw_mask, 5);
    //some erosion
    cv::morphologyEx(bw_mask, bw_mask, cv::MORPH_ERODE, se1, cv::Point(-1, -1));
    //detect blob
    //findContours(bw_mask, contoursGreen, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE, cv::Point(0, 0));

    cv::Mat biggest_cont;
    biggest_cont = bw_mask.clone();

    //invert image to get background
    biggest_cont = 255 - bw_mask.clone();
    //erode heavily  so that small blobs disappear and only big background areas should remain
    se1 = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(21, 21), cv::Point(-1, -1));
    cv::erode(biggest_cont, biggest_cont, se1, cv::Point(-1, -1), 3);

    //find contours again
    findContours(biggest_cont.clone(), contoursGreen, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE, cv::Point(0, 0));

    //check what percentage of total background is biggerblobs
    int white_pix = 0;
    //white_pix = cv::countNonZero(biggest_cont);
    double bg_area = 0;


    for (int i = 0; i < contoursGreen.size(); i++)
    {
      white_pix +=  cv::contourArea(contoursGreen[i]);
      if (cv::contourArea(contoursGreen[i]) > mParams.get()->GetBGBlobArea())
        bg_area += cv::contourArea(contoursGreen[i]);
    }

    bg_area = bg_area / double(white_pix);
    //std::cout<<"BG AREa    "<<bg_area<<std::endl;
    //If foreground has bigger blobs then its near the tail
    if (bg_area > mParams.get()->GetBackgroundRatio()) //should be 0.6
    {
      std::string region_tag = "VeryThin";
      //mObj->setFGVal(bg_area);
      mObj->setRegionTag(region_tag);
      return -1;

    }

    //calculate ratio of area of non ovrlapping cells to total foreground
    double total_fore_area1 = 0;
    double total_cell_area1 = 0;
    cv::Mat filled_mask1 = cv::Mat::zeros(bw_mask.size(), CV_8UC1);
    findContours(bw_mask.clone(), contoursGreen, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE, cv::Point(0, 0));

    for (int i = 0; i < contoursGreen.size(); i++)
    {
      if (contoursGreen[i].size() <= 5)
        continue;
      else
      {
        total_fore_area1 += cv::contourArea(contoursGreen[i]);
        cv::RotatedRect rect_ell = cv::fitEllipse(contoursGreen[i]);
        double majAx = std::max(rect_ell.size.width, rect_ell.size.height);
        majAx = majAx*mParams.get()->GetPix2UMFactor40x();
        cv::drawContours(filled_mask1, contoursGreen, i, cv::Scalar(255), cv::FILLED);
        if (majAx > mParams.get()->GetMinComponenSize() && majAx < mParams.get()->GetMaxComponenSize())
        {
          total_cell_area1 += cv::contourArea(contoursGreen[i]);

        }
        else
          continue;
      }


    }

    //if total area of non overlapping cells is too less
    double fg_ratio1 = total_cell_area1 / total_fore_area1;
    if (fg_ratio1 <= mParams.get()->GetForegroundRatio40x()) //0.3 for 40x
    {
      mObj->setFGVal(fg_ratio1);
      std::string region_tag = "ThickRegion";
      mObj->setRegionTag(region_tag);
      return -1;
    }
    else
    {
      //find all the centerpallors
      cv::multiply(255 - bw_mask, filled_mask1, filled_mask1);
      findContours(filled_mask1.clone(), contoursGreen, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE, cv::Point(0, 0));
      int num_pallor = 0;
      for (int i = 0; i < contoursGreen.size(); i++)
      {
        if (contoursGreen[i].size() <= 5)
          continue;
        else
        {
          cv::RotatedRect rect = cv::fitEllipse(contoursGreen[i]);
          double MajA = std::max(rect.size.width, rect.size.height);
          MajA = MajA*mParams.get()->GetPix2UMFactor40x();
          if (MajA > mParams.get()->GetMinCPSize() && MajA < mParams.get()->GetMaxCPSize())  //2&6
          {
            num_pallor++;
          }
        }

      }
      //if not enough center pallors, then its thin region
      if (num_pallor <= mParams.get()->GetPallorNumber40x())
      {
        std::string region_tag = "ThinRegion";
        mObj->setRegionTag(region_tag);

        return 1;
      }
      else  //monolayer region
      {
        std::string region_tag = "Monolayer";
        point mono_cen;
        mono_cen.x = ceil(left_white_corr.cols / 2);// +bbox.width / 2);
        mono_cen.y = ceil(left_white_corr.rows / 2);// +bbox.height / 2);
        std::vector<point> mono_points;
        mono_points.push_back(mono_cen);
        mObj->setRegionTag(region_tag);
        mObj->setMonoRegion(mono_points);
        mObj->setMonoVotes(9);
        mono_points.clear();
        return 2;
      }
    }
    return -1;
  }


  /*! \brief DetectCell
   *  Function to detect WBC's in an FOV
   *  Performs max entropy threshold on A channel
   *  Filter out some debris based on their diameter
   *  @_input :- cv::Mat image
   *  @_output :- WBCStruct which is returned to ProcessPipeline
   */
  template<class T>
  int WBCDetection<T>::DetectCell(void* _input, void* _output, int mag)
  {
    WBCStruct* mObj = static_cast<WBCStruct*>(_output);

    float pix2um = 0;
    if(mag==1)
      pix2um = mParams.get()->GetPix2UMFactor();
    else
      pix2um = mParams.get()->GetPix2UMFactor40x();

    cv::Mat* inpLeftImage = static_cast<cv::Mat*>(_input);//(size[1], size[0], CV_8UC3);
    if(mag==2)
    {
//      (*inpLeftImage) = 200*((*inpLeftImage)/white_image);
      (*inpLeftImage).convertTo((*inpLeftImage), CV_8UC3);
      cv::medianBlur((*inpLeftImage), (*inpLeftImage), 5);
    }
    // Copy the data to this buffer
    //memcpy(inpLeftImage.data, (*inpImage)->GetBufferPointer(),3*inpLeftImage.rows*inpLeftImage.cols);

    //convert to lab
    cv::Mat a_channel;
    std::vector<cv::Mat> chs;
    cv::cvtColor((*inpLeftImage), a_channel, cv::COLOR_BGR2Lab);
    cv::split(a_channel, chs);

    //do maxentropie threshold on a channel
    cv::Mat1b in,out;
    in = a_channel;
    MaxEntropyThreshold(chs[1], out);
    chs.clear();
    //convert back to opencv
    cv::Mat maxent_mask = out; //itk::OpenCVImageBridge::ITKImageToCVMat<UCharImage>(threshold->GetOutput());
    //check if image is inverted

    double non_zero = cv::countNonZero(maxent_mask);
    double zeropix = maxent_mask.rows*maxent_mask.cols - non_zero;
    if (zeropix < non_zero)
      maxent_mask = 255 - maxent_mask;

    int white_pix = cv::countNonZero(maxent_mask);
    double fg_ratio  = double(white_pix)/double(maxent_mask.rows*maxent_mask.cols);

    //check if foreground is greater than a threshold, reject that aoi
    //could be because of heavy debris or a bug in opencv lab conversion
    if(mag == 1)
    {
      if(fg_ratio > 0.15)
      {
        mObj->setNumberOfWBC(0);
        //allAOI.insert(std::pair<std::string, WBCStruct>(n, mObj));
        return -1;
      }
    }
    else
    {
      if(fg_ratio >= 0.15)
      {
        mObj->setNumberOfWBC(0);
        //allAOI.insert(std::pair<std::string, WBCStruct>(n, mObj));
        return -1;
      }
    }

    //we gonna clean the hell out of this image.
    cv::Mat se;
    if(mag==1)
      se = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3, 3), cv::Point(-1, -1));
    else
      se = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5), cv::Point(-1, -1));
    //median filtering
    cv::medianBlur(maxent_mask, maxent_mask, 3);
    cv::morphologyEx(maxent_mask, maxent_mask, cv::MORPH_OPEN, se, cv::Point(-1, -1));
    cv::morphologyEx(maxent_mask, maxent_mask, cv::MORPH_CLOSE, se, cv::Point(-1, -1));


    std::vector<std::vector<cv::Point>> contours;
    findContours(maxent_mask, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE, cv::Point(0, 0));

    float min_wbc_size = mParams.get()->GetMinWBCSize();
    float max_wbc_size = mParams.get()->GetMaxWBCSize();

    //some variabless
    int contour_count = 0;
    std::vector<point> wbc_cen;
    std::vector<float> wbc_area;
    std::vector<float> wbc_peri;
    std::vector<float> wbc_dia;
    std::vector<float> wbc_ecc;

    //find all the wbcs and calculate their shape features
    for (int i = 0; i < contours.size(); i++)
    {
      if (contours[i].size() < 5)
        continue;
      else
      {
        //fit ellipse to find the necessary shape features
        cv::RotatedRect ell = cv::fitEllipse(contours[i]);
        float maj_axis = std::max(ell.size.width, ell.size.height)*pix2um;
        float min_axis = std::min(ell.size.width, ell.size.height)*pix2um;
        if (maj_axis < max_wbc_size && maj_axis > min_wbc_size)
        {
          double wArea = cv::contourArea(contours[i])*pix2um*pix2um;
          std::vector<cv::Point> hull;
          cv::convexHull(contours[i], hull);
          double sol = cv::contourArea(contours[i]) / cv::contourArea(hull);
          //          //std::cout<<sol<<" : solidity"<<std::endl;
          if(min_axis/maj_axis > mParams.get()->GetEccThresh() && sol > mParams.get()->GetSolidityThresh())
          {
            //            //std::cout<<"exited from ecc check"<<std::endl;
            //cv::ellipse(left_white_corr, ell, cv::Scalar(0, 100, 355));
            double x_cen = 0;
            double y_cen = 0;
            //calculate centroids
            for (int j = 0; j < contours[i].size(); j++)
            {
              x_cen = x_cen + contours[i][j].x;
              y_cen = y_cen + contours[i][j].y;
            }
            x_cen = floor(x_cen / contours[i].size());
            y_cen = floor(y_cen / contours[i].size());
            contour_count++;
            if (contour_count != 0)
            {
              point wbc_p;
              wbc_p.x = x_cen;
              wbc_p.y = y_cen;
              wbc_cen.push_back(wbc_p); //centroid
              wbc_area.push_back(wArea); //area = pi*a*b
              wbc_peri.push_back(contours[i].size()*pix2um); //perimeter
              wbc_dia.push_back(maj_axis); //diameter
              wbc_ecc.push_back(min_axis / maj_axis); //eccentricity = min_axis / maj_axis;
              //              cv::drawContours((*inpLeftImage), contours, i, cv::Scalar::all(255), 3);

            }
          }
        }
      }
    }

    maxent_mask.release();
    out.release();
    in.release();
    contours.clear();
    se.release();

    if (contour_count == 0)
    {
      mObj->setNumberOfWBC(0);
      //allAOI.insert(std::pair<std::string, WBCStruct>(n, mObj));
      return -1;
    }
    else
    {
      mObj->setNumberOfWBC(contour_count);
      mObj->setWBCCentroid(wbc_cen);
      mObj->setWBCArea(wbc_area);
      mObj->setWBCPeri(wbc_peri);
      mObj->setWBCDia(wbc_dia);
      mObj->setWBCECC(wbc_ecc);
      //contour_count.clear();
      wbc_cen.clear();
      wbc_area.clear();
      wbc_peri.clear();
      wbc_dia.clear();
      wbc_ecc.clear();

      //allAOI.insert(std::pair<std::string, WBCStruct>(n, mObj));

      return 0;

    }
    return 0;

  }//end of function

  /*! \brief CompleteProcessing
   *  This function.....(describe it here)
   */
  template<class T>
  int WBCDetection<T>::CompleteProcessing()
  {
    //std::cout<<"Finishing Running plugin module"<<std::endl;
    boost::property_tree::ptree main_tag;
    boost::property_tree::ptree main_cell_pt;
    std::string all_cells_tag = "images";
    long cell_number = 0;

    // First check if the file exists
    std::string json_file_name = mParams->GetOutputFilePath();
    if (boost::filesystem::exists(json_file_name))
    {
      read_json(json_file_name, main_tag);
    }

    //sort the aoi data in ascending order
    //std::sort(allAoiInfo.begin(), allAoiInfo.end());


    for (auto const &cell_obj : allAOI)
    {
      std::string main_cell_tag = "image-" + std::to_string(cell_number);
      boost::property_tree::ptree cell_info_pt;

      std::string left_type = "empty";
      std::string right_type = "empty";

      WBCStruct cell = cell_obj.second;
      std::string aoi_name = cell.GetAoiName();

      //		fs::path cell_path(cell.image_name);
      cell_info_pt.put("aoi_name", aoi_name);
      //cell_info_pt.put("aoi_index", std::to_string(cell_obj.first));
      if(left_type == "empty" || right_type == "empty")
        cell_info_pt.put("region_type", cell.GetRegionType());
      else if(left_type == right_type)
        cell_info_pt.put("region_type", left_type);
      else
        cell_info_pt.put("region_type", cell.GetRegionType());
      cell_info_pt.put("foreground_ratio", std::to_string(cell.GetFGVal()));
      cell_info_pt.put("wbc_count", std::to_string(cell.GetWBCCount()));
      cell_info_pt.put("mono_votes", std::to_string(cell.GetMonoVotes()));
      cell_info_pt.put("TotalFG", std::to_string(cell.GetTotalFG()));
      cell_info_pt.put("TotalBG", std::to_string(cell.GetTotalBG()));


      std::vector<point> wCen = cell.GetWBCCentroid();
      std::vector<float> wArea = cell.GetWBCArea();
      std::vector<float> wPeri = cell.GetWBCPeri();
      std::vector<float> wDia = cell.GetWBCDia();
      std::vector<float> wEcc = cell.GetWBCECC();

      boost::property_tree::ptree wbc_cent, wbc_area, wbc_peri, wbc_dia, wbc_ecc;
      for (int j = 0; j < wCen.size(); j++)
      {
        boost::property_tree::ptree centroid;
        boost::property_tree::ptree area, peri, ecc, dia;
        std::string cent = std::to_string(wCen[j].x) + "," + std::to_string(wCen[j].y);
        centroid.put("", cent);
        area.put("", std::to_string(wArea[j]));
        peri.put("", std::to_string(wPeri[j]));
        dia.put("", std::to_string(wDia[j]));
        ecc.put("", std::to_string(wEcc[j]));

        wbc_cent.push_back(std::make_pair("", centroid));
        wbc_area.push_back(std::make_pair("", area));
        wbc_peri.push_back(std::make_pair("", peri));
        wbc_dia.push_back(std::make_pair("", dia));
        wbc_ecc.push_back(std::make_pair("", ecc));

      }


      cell_info_pt.add_child("wbc_centroid", wbc_cent);
      cell_info_pt.add_child("wbc_area", wbc_area);
      cell_info_pt.add_child("wbc_perimeter", wbc_peri);
      cell_info_pt.add_child("wbc_diameter", wbc_dia);
      cell_info_pt.add_child("wbc_eccentricity", wbc_ecc);
      boost::property_tree::ptree mono_cent;
      std::vector<point> mCen = cell.GetMonoCentroid();

      for (int j = 0; j < mCen.size(); j++)
      {
        boost::property_tree::ptree centroid;
        std::string cent = std::to_string(mCen[j].x) + "," + std::to_string(mCen[j].y);
        centroid.put("", cent);
        mono_cent.push_back(std::make_pair("", centroid));
      }
      cell_info_pt.add_child("mono_regions", mono_cent);

      //main_cell_pt.add_child(main_cell_tag, cell_info_pt);
      main_cell_pt.push_back(std::make_pair("", cell_info_pt));

      cell_number++;
    }
    std::cout<<" \n Entered DATA :"<<"\n";

    // Check if the main tag is found. If not, create the tag.
    boost::optional<bool> v = main_tag.get_optional<bool>(all_cells_tag);
    if (!v)
    {
      std::cout<<" not found :"<<v<<std::endl;
      main_tag.erase(all_cells_tag);
    }
    main_tag.add_child(all_cells_tag, main_cell_pt);
    boost::property_tree::json_parser::write_json(json_file_name, main_tag);
    return 1;
  }

  /*! \brief MaxEntropyThreshold
   *  Implementation of thresholding using max entropy
   *  aglorithm.
   *  See :- https://stackoverflow.com/questions/34362593/max-entropy-thresholding-using-opencv
   */
  template<class T>
  uchar WBCDetection<T>::MaxEntropyThreshold(const cv::Mat1b& src, cv::Mat1b& dst)
  {
    // Histogram
    cv::Mat1d hist(1, 256, 0.0);
    for (int r=0; r<src.rows; ++r)
      for (int c=0; c<src.cols; ++c)
        hist(src(r,c))++;

    // Normalize
    hist /= double(src.rows * src.cols);

    // Cumulative histogram
    cv::Mat1d cumhist(1, 256, 0.0);
    float sum = 0;
    for (int i = 0; i < 256; ++i)
    {
      sum += hist(i);
      cumhist(i) = sum;
    }

    cv::Mat1d hl(1, 256, 0.0);
    cv::Mat1d hh(1, 256, 0.0);

    for (int t = 0; t < 256; ++t)
    {
      // low range entropy
      double cl = cumhist(t);
      if (cl > 0)
      {
        for (int i = 0; i <= t; ++i)
        {
          if (hist(i) > 0)
          {
            hl(t) = hl(t) - (hist(i) / cl) * log(hist(i) / cl);
          }
        }
      }

      // high range entropy
      double ch = 1.0 - cl;  // constraint cl + ch = 1
      if (ch > 0)
      {
        for (int i = t+1; i < 256; ++i)
        {
          if (hist(i) > 0)
          {
            hh(t) = hh(t) - (hist(i) / ch) * log(hist(i) / ch);
          }
        }
      }
    }

    // choose best threshold
    cv::Mat1d entropie(1, 256, 0.0);
    double h_max = hl(0) + hh(0);
    uchar threshold = 0;
    entropie(0) = h_max;

    for (int t = 1; t < 256; ++t)
    {
      entropie(t) = hl(t) + hh(t);
      if (entropie(t) > h_max)
      {
        h_max = entropie(t);
        threshold = uchar(t);
      }
    }

    // Create output image
    dst = src > threshold;

    return threshold;

  }
}//end of namespace

template class spin::WBCDetection<cv::Mat>;
