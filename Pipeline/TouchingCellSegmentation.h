﻿#ifndef TOUCHINGCELLSEGMENTATION_H
#define TOUCHINGCELLSEGMENTATION_H

#include "opencv2/opencv.hpp"

class TouchingCellSegmentation
{
public:
  TouchingCellSegmentation() {}

  ~TouchingCellSegmentation() {}

  int SegmentTouchingCells(const cv::Mat &img, cv::Mat &mask,
                           std::vector<cv::Point> contour, cv::Mat &out);

private:
  int ComputeDistTransform(const cv::Mat &img, const cv::Mat& inp,
                           std::vector<cv::Point> contours, cv::Mat &out);

  int Watershed(cv::Mat &img, const cv::Mat &mask,
                 std::vector<std::vector<cv::Point>> cont,
                 cv::Mat &out);

  int ComputeFRST(const cv::Mat &inp, cv::Mat &out);

};
#endif // TOUCHINGCELLSEGMENTATION_H
