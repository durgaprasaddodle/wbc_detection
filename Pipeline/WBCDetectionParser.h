#ifndef WBCDETECTIONPARSER_H_
#define WBCDETECTIONPARSER_H_
#include <memory>

namespace spin
{
  struct WBCDetectionParams;

  class WBCDetectionParser
  {
    public:
      // Default constructor
      WBCDetectionParser();
      // Overloaded constructor
      WBCDetectionParser(void* inp);

      //Default parameter parser from a file
      int ParseParams(const char* inpFilePath);
      //Debug routine
      int DisplayParams();

      //some getters to access params
      std::string GetWhitePath();
      std::string GetOutputFilePath();
      std::string GetDebugPath();
      float       GetMinWBCSize();
      float       GetMaxWBCSize();
      float       GetMinVotes();
      float       GetPallorNumber();
      float       GetPallorNumber40x();
      float       GetForegroundRatio();
      float       GetForegroundRatio40x();
      float       GetMinComponenSize();
      float       GetMaxComponenSize();
      float       GetNumberOfPeaks();
      float       GetPeakThresh();
      std::string GetInputImagePath();
      double      GetForegroundThresh();
      double      GetForegroundThresh40x();
      double      GetPix2UMFactor();
      double      GetPix2UMFactor40x();
      int         GetHistBins();
      int         GetNumberofGrids();
      float       GetBackgroundRatio();
      int         GetMinCPSize();
      int         GetMaxCPSize();
      int         GetBGBlobArea();
      int         GetKernelSize();
      float       GetUnsharpFactor();
      int         GetDerisSize();
      std::string GetMagnification();
      float       GetEccThresh();
      float       GetMinWBCArea();
	    float       GetSolidityThresh();
      void        SetWBCDetectionParams(void* inp);
      const WBCDetectionParams* GetParams(){return mWBCParams.get();}
    private:
      std::shared_ptr<WBCDetectionParams> mWBCParams;
  };//end of class
};

#endif // !_PARAMETER_PARSER_
