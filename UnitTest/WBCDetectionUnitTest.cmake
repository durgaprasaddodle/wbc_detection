ADD_EXECUTABLE(UnitTest_WBCDetection
										${PROJECT_SOURCE_DIR}/UnitTest/WBCDetectionUnitTest.cpp
									    ${PROJECT_SOURCE_DIR}/UnitTest/WBCDetectionUnitTest.h)
TARGET_LINK_LIBRARIES(UnitTest_WBCDetection SpinWBC ${EXT_LIBS})
SET_TARGET_PROPERTIES(UnitTest_WBCDetection
						PROPERTIES  COMPILE_FLAGS " -D_DENSE_REP -DSPIN_EXPORTS"
									DEBUG_POSTFIX "_d")     
#===============================================================================
#We copy all necessary Run time libraries into the folder containing the
#executable. 
#===============================================================================
IF(MSVC)
ADD_CUSTOM_COMMAND(
  TARGET UnitTest_WBCDetection
  POST_BUILD 
  COMMAND ${CMAKE_COMMAND} 
         -DTARGETDIR=$<TARGET_FILE_DIR:UnitTest_WBCDetection> 
         -DRELEASE_LIBS=${RELEASE_LIBS_TO_BE_COPIED}
         -DDEBUG_LIBS=${DEBUG_LIBS_TO_BE_COPIED}
         -DCFG=${CMAKE_CFG_INTDIR} -P "${LIBRARY_ROOT}/PostBuild.cmake"
)
ENDIF()
