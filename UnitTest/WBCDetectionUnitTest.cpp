#include "Pipeline/WBCDetection.h"
#include "opencv2/opencv.hpp"
#include "boost/filesystem.hpp"
#include "boost/timer/timer.hpp"
#include "boost/foreach.hpp"
#include <opencv2/core/ocl.hpp>

using namespace spin;
namespace fs = boost::filesystem;
namespace btimer = boost::timer;

int main(int argc, char* argv[])
{
  if (argc != 3)
  {
    std::cout << "/EXE" << std::endl;
    std::cout << "/ Absolute Path to Parameter File" << std::endl;
    std::cout << "/Absolute Path to input image" << std::endl;
    return -1;
  }
  cv::ocl::setUseOpenCL(false);

  btimer::cpu_timer timer;
  std::shared_ptr<WBCDetection<cv::Mat>> mObj = \
  std::make_shared<WBCDetection<cv::Mat>>();
  mObj->InstantiatePipeline(argv[1]);
  if (false)
  {
    timer.start();
    cv::Mat im = cv::imread(argv[2]);
    fs::path img_path(argv[2]);

    std::cout<<mObj->ProcessPipeline(&im, img_path.filename().stem().string());
    //char* a = "1";
    mObj->CompleteProcessing();
    timer.stop();
    std::cout << "Pipeline Timing : " << timer.elapsed().wall / 1e9 << std::endl;
  }

  //if you want to run on folder of images set it to true and comment the above code
  else
  {
    std::vector<cv::Mat> images;;
    std::vector < std::string> names;
    timer.start();
    fs::path targetDir(argv[2]);

    fs::directory_iterator it(targetDir), eod;
    BOOST_FOREACH(fs::path const &p, std::make_pair(it, eod))
    {
      if (fs::is_regular_file(p))
      {
        // do something with p
        if (("background" != p.filename().stem()) && (".png" == p.extension()) || (".bmp" == p.extension()) || \
        (".jpg" == p.extension()) || (".jpeg" == p.extension()))
        {
          cv::Mat im = cv::imread(p.string());
          //fs::path img_path(argv[2]);
          //images.push_back(im);
          //names.push_back(p.filename().stem().string());
          mObj->ProcessPipeline(&im, p.filename().stem().string());
          //std::cout << p.filename() << std::endl;
        }
      }
    }
    mObj->CompleteProcessing();

/*#pragma omp parallel for
    for(int i=0;i<images.size(); i++)
      mObj->ProcessPipeline(&images[i],names[i]);
    timer.stop();
    char* a;
    mObj->CompleteProcessing();
    std::cout << "Pipeline Timing : " << timer.elapsed().wall / 1e9 << std::endl;
    */
  }

  return 0;
}
