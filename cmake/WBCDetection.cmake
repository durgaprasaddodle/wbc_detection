#------------------------------------------------------------------------------#
# Raise Flag to link with other libraries
#------------------------------------------------------------------------------#
# SET(EXT_LINK ON CACHE BOOL "Link with other libraries")

INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR})

SET(WBC_Folders
         Pipeline)
		
SET(WBC_SRCS "")

FOREACH(folder ${WBC_Folders})
    FILE(GLOB_RECURSE tSRCS ${PROJECT_SOURCE_DIR}/${folder}/*.cpp
                            ${PROJECT_SOURCE_DIR}/${folder}/*.h
                            ${PROJECT_SOURCE_DIR}/${folder}/*.hpp
                            ${PROJECT_SOURCE_DIR}/${folder}/*.txx)
    SET(WBC_SRCS ${WBC_Folders} ${tSRCS})
ENDFOREACH()


#===============================================================================
#Compile relevant files as Object files
#===============================================================================
ADD_LIBRARY(spinWBC_aux OBJECT ${WBC_SRCS})

#------------------------------------------------------------------------------#
# Get the third party libraries linked
#-----------------------------------------------------------------------------

SET(NonModular_List
  openmp
#  Armadillo
)


SET(Modular_List
    OpenCV
    ITK
    boost-1_64
)


# Select the libraries
FOREACH(mLib ${NonModular_List})
    SET(NonModularLibrary_${mLib} ON CACHE BOOL "Use ${mLib} Library" FORCE)
ENDFOREACH()

FOREACH(mLib ${Modular_List})
    SET(ModularLibrary_${mLib} ON CACHE BOOL "Use ${mLib} Library" FORCE)
ENDFOREACH()



##############################################################################
SET(ITK_MANDATORY_PACKAGES
    IONRRD
    IOPNG
    PNG
    IOBMP
    LabelMap
    ImageLabel
    Statistics
    ImageStatistics
    ImageFilterBase
    VideoIO
	)
FOREACH(mLib ${ITK_MANDATORY_PACKAGES})
  SET(ITK_${mLib}_ENABLE ON CACHE BOOL "Use ${mLib} Library" FORCE)
ENDFOREACH()
##############################################################################
SET(Boost_MANDATORY_PACKAGES
    filesystem
    #thread
    date_time
    chrono
    system
    timer
    serialization
)
FOREACH(mLib ${Boost_MANDATORY_PACKAGES})
    SET(Boost_${mLib}_ENABLE ON CACHE BOOL "Use ${mLib} Library" FORCE)
ENDFOREACH()
##############################################################################
SET(OpenCV_MANDATORY_PACKAGES
       core
       highgui
       imgproc
       imgcodecs
       xfeatures2d
)
FOREACH(mLib ${OpenCV_MANDATORY_PACKAGES})
    SET(OpenCV_${mLib}_ENABLE ON CACHE BOOL "Use ${mLib} Library" FORCE)
ENDFOREACH()

SET(MODULAR_MANDATORY_LIB_LIST ITK OpenCV)

##############################################################################
IF(MSVC)
FIND_PACKAGE(ThirdPartyLibraries_VS2015 REQUIRED)
ELSE()
	INCLUDE(${CMAKE_SOURCE_DIR}/cmake/TPL_Linux_WBCDetection.cmake)
ENDIF()

#------------------------------------------------------------------------------#
# Get the main sources of this pipeline and add them to this library
#------------------------------------------------------------------------------#
ADD_LIBRARY(SpinWBC
	$<TARGET_OBJECTS:spinWBC_aux>

    ${PROJECT_SOURCE_DIR}/Pipeline/TouchingCellSegmentation.cpp
    ${PROJECT_SOURCE_DIR}/Pipeline/TouchingCellSegmentation.h
    ${PROJECT_SOURCE_DIR}/Pipeline/FRST.h
    ${PROJECT_SOURCE_DIR}/Pipeline/WBCDetection.cpp
    ${PROJECT_SOURCE_DIR}/Pipeline/WBCDetection.h

    )

TARGET_LINK_LIBRARIES(SpinWBC
                        ${EXT_LIBS})
SET_TARGET_PROPERTIES(SpinWBC
                                PROPERTIES  COMPILE_FLAGS "-DSPIN_EXPORTS"
                                DEBUG_POSTFIX "_d")
IF(MSVC)
    ADD_CUSTOM_COMMAND(
        TARGET SpinWBC
        POST_BUILD
        COMMAND ${CMAKE_COMMAND}
        -DTARGETDIR=$<TARGET_FILE_DIR:SpinWBC>
        -DRELEASE_LIBS=${RELEASE_LIBS_TO_BE_COPIED}
        -DDEBUG_LIBS=${DEBUG_LIBS_TO_BE_COPIED}
        -DCFG=${CMAKE_CFG_INTDIR} -P "${LIBRARY_ROOT}/PostBuild.cmake"
    )
ENDIF()
# #------------------------------------------------------------------------------#
# # Unit Test for the WBC module
# #------------------------------------------------------------------------------#

# #------------------------------------------------------------------------------#
# # Unit Test for the WBCPipeline module
# #------------------------------------------------------------------------------#

SET(UnitTest_WBCDetection ON CACHE BOOL "Build UnitTest_WBCPipelineUnitTest module")
IF(UnitTest_WBCDetection)
    INCLUDE(${PROJECT_SOURCE_DIR}/UnitTest/WBCDetectionUnitTest.cmake)
ENDIF(UnitTest_WBCDetection)
